FROM python:3.6
MAINTAINER Surkal <https://gitlab.com/Surkal>

ENV C_FORCE_ROOT 1

# Create unprivileged user
RUN useradd docker

# Install PostgreSQL dependencies
RUN apt-get update && \
    apt-get install -y apt-utils postgresql-client libpq-dev memcached gettext && \
    rm -rf /var/lib/apt/lists/*

ENV PYTHONBUFFERED 1

# Default port the webserver runs on
EXPOSE 8000

RUN mkdir /config

# Working directory for the application
ENV APPLICATION_ROOT /src/
RUN mkdir -p $APPLICATION_ROOT
WORKDIR $APPLICATION_ROOT

# Install required modules
ADD requirements.txt $APPLICATION_ROOT
RUN pip install -r requirements.txt

ADD . $APPLICATION_ROOT
