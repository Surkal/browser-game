build:
	docker-compose build

runserver:
	docker-compose up

test:
	docker-compose run web python manage.py test

shell:
	docker-compose run web python manage.py shell
