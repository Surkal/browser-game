from django import forms
from django.db.models import Q

from core.models import Product


def get_all_products():
    return Product.objects.all()

def get_product_list(activity):
    cat = {
        'farm': 'agri',
        'exploit': 'exploitation',
    }
    if activity == 'factory':
        return Product.objects.filter(Q(category='agro') | Q(category='industrie'))
    return Product.objects.filter(category=cat[activity])


class NewOfferForm(forms.Form):
    BUY = 'buy'
    SELL = 'sell'
    ACTIONS_CHOICES = (
        (BUY, 'Purchase'),
        (SELL, 'Sell')
    )
    action = forms.ChoiceField(choices=ACTIONS_CHOICES)
    product = forms.ModelChoiceField(queryset=get_all_products(), required=True)
    quantity = forms.IntegerField(max_value=9999999999, min_value=1, required=True)
    price = forms.DecimalField(max_digits=9, decimal_places=2, min_value=0, required=True)


class PaymentForm(forms.Form):
    quantity = forms.IntegerField(
        max_value=9999999999,
        min_value=1,
        required=True,
        label=""
    )


class BuyFieldForm(forms.Form):
    product = forms.ModelChoiceField(queryset=Product.objects.all())

    def __init__(self, *args, **kwargs):
        activity = kwargs.pop('activity', '')
        super(BuyFieldForm, self).__init__(*args, **kwargs)
        products = get_product_list(activity)
        self.fields['product'].queryset = products
