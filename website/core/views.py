from decimal import Decimal

from django.http import Http404
from django.urls import reverse
from django.conf import settings
from django.db import transaction
from django.db.models import F, Q
from django.contrib import messages
from django.shortcuts import render
from django.contrib.auth.models import User
from django.views.generic.edit import FormView
from django.views.generic.list import ListView
from django.utils.translation import ugettext as _
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin

from core.forms import NewOfferForm, PaymentForm, BuyFieldForm
from core.models import Player, Product, Ownership, Offer, Transaction, Production


def home(request):
    return render(request, 'base.html')


class ItemOffersView(SuccessMessageMixin, LoginRequiredMixin, ListView, FormView):
    template_name = 'market.html'
    form_class = PaymentForm

    def post(self, request, *args, **kwargs):
        qty = int(request.POST.get('quantity'))
        action = self.kwargs['action']
        # Find the offer id (primary key)
        for key in request.POST.keys():
            try:
                pk = int(key)
            except ValueError:
                pass
        offer = Offer.objects.get(pk=pk)

        if action == 'buy':
            buyer = request.user
            seller = offer.player.user
        else:
            buyer = offer.player.user
            seller = request.user

        # Can't exceed the offer quantity
        if qty > offer.quantity:
            qty = offer.quantity

        # Transaction amount
        total = offer.price * qty

        # Can't buy to yourself
        if buyer == seller:
            self.message = _(f"You can't {action} to yourself.")
            return self.form_invalid(self, **kwargs)

        # Check if the seller has enough product quantity
        if action == 'sell':
            try:
                owned = Ownership.objects.get(player__user=seller, product=offer.product)
            except ObjectDoesNotExist:
                self.message = _("You don't have this ressource.")
                return self.form_invalid(self, **kwargs)
            if not owned.quantity >= qty:
                self.message = _("You don't have enough ressources.")
                return self.form_invalid(self, **kwargs)

        buyer_ = Player.objects.get(user=buyer)
        if action == 'buy':
            if not buyer_.money >= total:
                self.message = _("You don't have enough money.")
                return self.form_invalid(self, **kwargs)
        else:
            if not offer.player.money >= total:
                self.message = _("The buyer doesn't have enough money.")
                return self.form_invalid(self, **kwargs)

        with transaction.atomic():
            # Money transactions
            seller_ = Player.objects.get(user=seller)
            seller_.money += total
            seller_.save()
            buyer_.money -= total
            buyer_.save()

            # Product transactions
            if action == 'sell':
                owned.quantity -= qty
                owned.save()
                owned.refresh_from_db()
            if action == 'buy':
                p = Player.objects.get(user=request.user)
                bought, __ = Ownership.objects.get_or_create(player=p, product=offer.product)
            else:
                bought, __ = Ownership.objects.get_or_create(player=offer.player, product=offer.product)
            bought.quantity += qty
            bought.save()
            offer.quantity -= qty
            offer.save()
            offer.refresh_from_db()

            t = Transaction(
                seller=seller_,
                buyer=buyer_,
                product=offer.product,
                quantity=qty,
                price=offer.price,
            )
            t.save()

            # Clean stocks
            if offer.quantity < 1:
                offer.delete()

            if action == 'sell':
                if owned.quantity < 1:
                    owned.delete()
        return super(ItemOffersView, self).post(request, *args, **kwargs)

    def get_success_url(self):
        return self.request.path

    def get_queryset(self):
        action = self.kwargs['action']
        a = ['buy', 'sell']
        if not action in a:
            raise Http404("wrong url")
        a.remove(action)
        action = a[0]

        item = self.kwargs['item'].replace('-', ' ')
        try:
            self.p = Product.objects.get(name=item)
        except ObjectDoesNotExist:
            raise Http404("wrong product name in url")
        return Offer.objects.filter(status=action, product=self.p)

    def get_success_message(self, cleaned_data):
        if self.kwargs['action'] == 'buy':
            return _("You bought the item with success.")
        else:
            return _("You sold the item with success.")

    def form_invalid(self, form, **kwargs):
        messages.warning(self.request, self.message)
        form = PaymentForm()
        self.object_list = self.get_queryset()
        return super(ItemOffersView, self).form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super(ItemOffersView, self).get_context_data(**kwargs)
        context['action'] = self.kwargs['action']
        context['rate'] = self.p.price
        return context


class OffersView(LoginRequiredMixin, ListView):
    model = Product
    template_name = 'offers.html'

    def get(self, request, *args, **kwargs):
        if not self.kwargs['action'] in ('sell', 'buy'):
            raise Http404("wrong url")
        return super(OffersView, self).get(request, *args, **kwargs)

    def _get_offers_ids(self, action):

        a = ['buy', 'sell']
        a.remove(action)
        action = a[0]

        offers_nb = {}
        for x in self.object_list:
            offers_nb[x.pk] = Offer.objects.filter(status=action, product=x.pk).count()
        return offers_nb

    def get_context_data(self, **kwargs):
        context = super(OffersView, self).get_context_data(**kwargs)
        context['offers_nb'] = self._get_offers_ids(self.kwargs['action'])
        context['action'] = self.kwargs['action']
        return context


class NewOfferView(SuccessMessageMixin, LoginRequiredMixin, FormView):
    template_name = 'new_offer.html'
    form_class = NewOfferForm
    success_url = '/new_offer/'  # reverse_lazy doesn't work
    success_message = _("The offer has been posted.")

    def post(self, request, *args, **kwargs):
        player = Player.objects.get(user=request.user)
        product = Product.objects.get(id=request.POST.get('product'))
        action = request.POST.get('action')

        if action == 'sell':
            # Check if the player has the product
            try:
                owned = Ownership.objects.get(player=player, product=product)
            except ObjectDoesNotExist:
                self.message = _("You don't have this product in your stock.")
                return self.form_invalid(self, **kwargs)

            # Check if the player has enough product quantity
            sell_qty = int(request.POST.get('quantity'))
            if sell_qty > owned.quantity:
                self.message = _("You don't have enough ressources.")
                return self.form_invalid(self, **kwargs)

        # limit offers number per player
        offers_max = settings.OFFERS_NB_MAX
        if Offer.objects.filter(player=player,
                                product=product,
                                status=action).count() >= offers_max:
            self.message = _(f"You can't post more than {offers_max} offers.")
            return self.form_invalid(self, **kwargs)

        # Post the offer
        offer = Offer(
            player=player,
            product=product,
            status=request.POST.get('action'),
            quantity=request.POST.get('quantity'),
            price=request.POST.get('price'),
        )

        with transaction.atomic():
            offer.save()
            if request.POST.get('action') == 'sell':
                Ownership.objects.select_for_update()\
                                 .filter(player=player, product=product)\
                                 .update(quantity=F('quantity') - sell_qty)

        return super(NewOfferView, self).post(request, *args, **kwargs)

    def form_invalid(self, form, **kwargs):
        context = self.get_context_data(**kwargs)  # TODO: looks useless ?
        messages.warning(self.request, self.message)
        form = NewOfferForm()
        return super(NewOfferView, self).form_invalid(form)


class StockView(LoginRequiredMixin, ListView):
    model = Ownership
    template_name = 'stocks.html'

    def get_queryset(self):
        player = Player.objects.get(user=self.request.user)
        return Ownership.objects.filter(player=player)


# TODO: Make a pagination
class TransactionLogView(LoginRequiredMixin, ListView):
    template_name = 'transaction_log.html'

    def get_queryset(self):
        transactions = Transaction.objects.filter(
            Q(buyer__user=self.request.user) |
            Q(seller__user=self.request.user)
        )
        return transactions


class ProductionView(LoginRequiredMixin, ListView):
    template_name = 'production.html'

    def get_queryset(self, **kwargs):
        fields = Production.objects.filter(activity=self.kwargs['activity'], player__user=self.request.user)
        if not fields and not self.kwargs['activity'] in ['farm', 'factory', 'exploit']:
            raise Http404
        return fields

    def get_context_data(self, **kwargs):
        context = super(ProductionView, self).get_context_data(**kwargs)
        player = Player.objects.get(user=self.request.user)
        context['field_price'] = player.get_field_price
        return context


class BuyFieldView(SuccessMessageMixin, LoginRequiredMixin, FormView):
    template_name = 'buy_field.html'
    success_message = _("You bought a field.")
    form_class = BuyFieldForm

    def get(self, request, *args, **kwargs):
        activities = ('farm', 'factory', 'exploit')
        if not self.kwargs['activity'] in activities:
            raise Http404("wrong url")
        return super(BuyFieldView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        with transaction.atomic():
            player = Player.objects.get(user=request.user)
            # Check if the player has enough money
            field_price = Decimal(player.get_field_price)
            if player.money < field_price:
                self.message = _("You don't have enough money.")
                return self.form_invalid()

            player.money -= field_price
            player.fields += 1
            player.save()

            product = Product.objects.get(id=request.POST.get('product'))
            p = Production(
                activity = self._get_activity(product.category),
                product = product,
                player = player,
            )
            p.save()
        return super(BuyFieldView, self).post(request, *args, **kwargs)

    def form_invalid(self):
        messages.warning(self.request, self.message)
        form = BuyFieldForm
        return super(BuyFieldView, self).form_invalid(form)

    def get_success_url(self):
        return reverse('production', kwargs={'activity': self.kwargs['activity']})

    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super(BuyFieldView, self).get_form_kwargs(*args, **kwargs)
        kwargs['activity'] = self.kwargs['activity']
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(BuyFieldView, self).get_context_data(**kwargs)
        player = Player.objects.get(user=self.request.user)
        context['field_price'] = player.get_field_price
        return context

    def _get_activity(self, category):
        if category == 'agri':
            return 'farm'
        elif category == 'agro' or category == 'industrie':
            return 'factory'
        else:
            return 'exploit'
