from django.conf import settings
from django.utils.text import slugify
from django.contrib.auth.models import User
from django.db import models, IntegrityError


class Player(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    money = models.DecimalField(max_digits=19, decimal_places=3, default=10000000)
    fields = models.IntegerField(default=0)

    def __str__(self):
        return self.user.username

    @property
    def get_field_price(self):
        """
        Set the price of the next field to buy.
        """
        initial = settings.INITIAL_FIELD_PRICE
        field_inflation = settings.FIELD_INFLATION
        return round(initial * field_inflation ** self.fields, 2)


class Product(models.Model):
    CATEGORY_CHOICES = (
        ('agri', 'Agriculture'),
        ('agro', 'Agroalimentaire'),
        ('industrie', 'Industrie'),
        ('exploitation', 'Exploitation'),
    )

    name = models.CharField(max_length=50, unique=True)
    price = models.DecimalField(max_digits=9, decimal_places=2)
    category = models.CharField(choices=CATEGORY_CHOICES, max_length=40)
    productivity = models.IntegerField(default=10**5)
    ingredients = models.ManyToManyField(
        'self',
        through='ProductComponent',
        related_name="components",
        symmetrical=False,
        blank=True
    )

    class Meta:
        ordering = ['category', 'name']

    def __str__(self):
        return self.name

    @property
    def slug(self):
        return slugify(self.name)

    def get_ingredients(self):
        return list(p for p in self.ingredients.all())


class ProductComponent(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='product')
    ingredient = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='ingredient')
    quantity = models.DecimalField(max_digits=10, decimal_places=5)

    class Meta:
        ordering = ['product']

    def __str__(self):
        return f'{self.product} {self.ingredient} {self.quantity}'


class Ownership(models.Model):
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.DecimalField(max_digits=17, decimal_places=5, default=0)

    class Meta:
        ordering = ['player', 'product']

    def __str__(self):
        return f'{self.player.user} {self.product} {self.quantity}'

    def save(self, *args, **kwargs):
        if not self.quantity < 0:
            super(Ownership, self).save(*args, **kwargs)
        else:
            raise IntegrityError


class Offer(models.Model):
    BUY = "buy"
    SELL = "sell"
    STATUS_CHOICES = (
        (BUY, 'Buy'),
        (SELL, 'Sell')
    )

    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    status = models.CharField(choices=STATUS_CHOICES, max_length=10)
    quantity = models.DecimalField(max_digits=14, decimal_places=2)
    price = models.DecimalField(max_digits=9, decimal_places=3)
    created_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['status', 'product', 'price']

    def __str__(self):
        return f'{self.player.user} {self.status} {self.product.name} {self.quantity} {self.price}'

    @property
    def slug(self):
        return slugify(self.product.name)


class Transaction(models.Model):
    seller = models.ForeignKey(Player, on_delete=models.CASCADE, related_name='seller')
    buyer = models.ForeignKey(Player, on_delete=models.CASCADE, related_name='buyer')
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.DecimalField(max_digits=14, decimal_places=2)
    price = models.DecimalField(max_digits=9, decimal_places=3)
    created_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-created_date']

    @property
    def total(self):
        return quantity * price


class Production(models.Model):
    ACTIVITY_CHOICES = (
        ('farm', 'Farm'),
        ('factory', 'Factory'),
        ('exploit', 'Exploitation')
    )

    activity = models.CharField(choices=ACTIVITY_CHOICES, max_length=20)
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.activity} {self.player} {self.product}'


class GameCompany(models.Model):
    name = models.CharField(max_length=35)

    def __str__(self):
        return self.name
