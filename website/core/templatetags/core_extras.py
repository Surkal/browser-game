from django import template


register = template.Library()

@register.filter
def lookup(d, key):
    """Filter to access the value of a dictionary."""
    return d[key]

@register.filter
def nospace(value):
    """Convert int to str"""
    return str(value)
