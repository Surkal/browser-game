from django.contrib import admin

from .models import Player, Product, Ownership, Offer, Transaction, ProductComponent, Production, GameCompany


class PlayerAdmin(admin.ModelAdmin):
    list_display = ('user', 'money')


class ProductAdmin(admin.ModelAdmin):
    list_display = ('name', 'price', 'category', 'productivity', 'get_ingredients')


class ProductComponentAdmin(admin.ModelAdmin):
    list_display = ('product', 'ingredient', 'quantity')


class OwnershipAdmin(admin.ModelAdmin):
    list_display = ('player', 'product', 'quantity')


class OfferAdmin(admin.ModelAdmin):
    list_display = ('status', 'player', 'product', 'quantity', 'price', 'created_date')


class TransactionAdmin(admin.ModelAdmin):
    list_display = ('created_date', 'seller', 'buyer', 'product', 'quantity', 'price')


class ProductionAdmin(admin.ModelAdmin):
    list_display = ('activity', 'player', 'product')

class GameCompanyAdmin(admin.ModelAdmin):
    list_display = ('name',)


admin.site.register(Player, PlayerAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(ProductComponent, ProductComponentAdmin)
admin.site.register(Ownership, OwnershipAdmin)
admin.site.register(Offer, OfferAdmin)
admin.site.register(Transaction, TransactionAdmin)
admin.site.register(Production, ProductionAdmin)
admin.site.register(GameCompany, GameCompanyAdmin)
