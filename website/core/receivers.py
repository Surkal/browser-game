from django.contrib.auth.models import User

from django.dispatch import receiver
from django.db.utils import DatabaseError
from django.db.models.signals import post_save, pre_save

from core.models import Player, Ownership


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    """
    Create automatically a Player account after to register.
    """
    if created:
        Player.objects.create(user=instance)
