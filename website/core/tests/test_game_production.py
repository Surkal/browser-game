from django.test import TestCase
from django.contrib.auth.models import User

from core import game_production
from ..models import Product, Production, Ownership, Player, ProductComponent


class ProductionTests(TestCase):
    def setUp(self):
        self.p = Product.objects.create(name="lait", price=0.3, category='agri', productivity=10**5)
        self.usr = User.objects.create_user(username='john')
        self.pl = Player.objects.get(user=self.usr)
        p2 = Product.objects.create(name="plastique", category='industrie', productivity=3*10**5, price=2)
        Ownership.objects.create(player=self.pl, product=p2, quantity=10**4)
        p3 = Product.objects.create(name="lait en bouteille", category='agro', productivity=3*10**5, price=0.8)
        ProductComponent.objects.create(product=p3, ingredient=self.p, quantity=1)
        ProductComponent.objects.create(product=p3, ingredient=p2, quantity=0.03)
        for _ in range(5):
            Production.objects.create(product=self.p, player=self.pl, activity='farm')
        Production.objects.create(product=p3, player=self.pl, activity='factory')
        self.p2 = p2
        self.p3 = p3

    def test_farm_production(self):
        game_production.main()
        self.assertEqual(Ownership.objects.get(product=self.p, player=self.pl).quantity, 2*10**5) # + 500 000 - 300 000 (1 factory) = 200 000 lait
        self.assertEqual(Ownership.objects.get(product=self.p2, player=self.pl).quantity, 10**3)

    def test_not_enough_ressources(self):
        Production.objects.create(product=self.p3, player=self.pl, activity='factory')
        game_production.main()  # Lack of 8 000 plastiques and 100 000 lait
        self.assertEqual(Ownership.objects.get(product=self.p, player=self.pl).quantity, 2*10**5)
        self.assertEqual(Ownership.objects.get(product=self.p2, player=self.pl).quantity, 10**3)
        self.assertEqual(Ownership.objects.get(product=self.p3, player=self.pl).quantity, 3*10**5)

    def test_lack_of_one_ressource_plastique(self):
        Production.objects.create(product=self.p3, player=self.pl, activity='factory')
        Production.objects.create(product=self.p, player=self.pl, activity='farm')  # + 100 000 lait
        game_production.main()  # Lack of 8 000 plastiques
        self.assertEqual(Ownership.objects.get(product=self.p, player=self.pl).quantity, 3*10**5)
        self.assertEqual(Ownership.objects.get(product=self.p2, player=self.pl).quantity, 10**3)
        self.assertEqual(Ownership.objects.get(product=self.p3, player=self.pl).quantity, 3*10**5)

    def test_lack_of_one_ressource_lait(self):
        Production.objects.create(product=self.p2, player=self.pl, activity='factory')
        Production.objects.create(product=self.p3, player=self.pl, activity='factory')
        game_production.main()  # Lack of 100 000 lait
        self.assertEqual(Ownership.objects.get(product=self.p, player=self.pl).quantity, 2*10**5)
        self.assertEqual(Ownership.objects.get(product=self.p2, player=self.pl).quantity, 301*10**3)
        self.assertEqual(Ownership.objects.get(product=self.p3, player=self.pl).quantity, 3*10**5)
