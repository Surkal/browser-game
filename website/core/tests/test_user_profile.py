from django.test import TestCase
from django.contrib.auth.models import User

from ..models import Player


class UserCreationTests(TestCase):
    def test_new_user(self):
        self.assertFalse(Player.objects.all().exists())
        usr = User.objects.create_user(username='azerty')
        self.assertTrue(Player.objects.filter(user=usr).exists())
