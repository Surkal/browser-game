from random import randint

from django.test import TestCase

from ..templatetags.core_extras import lookup, nospace


class LookupTests(TestCase):
    def test_lookup_filter(self):
        for _ in range(10):
            x, y, z = randint(0, 99999), randint(0, 99999), randint(0, 99999)
            d = {'a': x, 'b': y, 'c': z}
            self.assertEqual(lookup(d, 'a'), x)
            self.assertEqual(lookup(d, 'b'), y)
            self.assertEqual(lookup(d, 'c'), z)


class NospaceTests(TestCase):
    def test_nospace_filter(self):
        x = randint(0, 999999999)
        self.assertEqual(nospace(123), '123')
        self.assertEqual(nospace(123456789), '123456789')
        self.assertIsInstance(nospace(123), str)
        self.assertIsInstance(nospace(123456789), str)
        self.assertIsInstance(nospace(x), str)
