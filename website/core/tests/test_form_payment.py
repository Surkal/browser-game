from random import randint

from django.urls import reverse
from django_webtest import WebTest
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from django.test import TestCase, override_settings
from django.core.exceptions import ObjectDoesNotExist

from ..forms import PaymentForm
from ..models import Offer, Product, Player, Ownership


class PaymentFormTests(TestCase):
    def setUp(self):
        user = get_user_model().objects.create_user('zoidberg')
        self.quantity = randint(1, 9999999999)

    def test_valid_data(self):
        form = PaymentForm({'quantity': self.quantity})
        self.assertTrue(form.is_valid())

    @override_settings(LANGUAGE_CODE='en')
    def test_blank_data(self):
        form = PaymentForm({})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {
            'quantity': ['This field is required.'],
        })


class BuyItemOffersViewTests(WebTest):
    def setUp(self):
        self.usr = User.objects.create_user(username='alpha', email='john@doe.com', password='top_secret')
        self.player_ = Player.objects.get(user=self.usr)
        user = User.objects.create_user(username='john', email='john@doe.com', password='top_secret')
        player = Player.objects.get(user=user)
        self.p = Product.objects.create(name='potatoe', price=1.00, category='agri')
        Ownership.objects.create(product=self.p, player=player, quantity=2000)
        Offer.objects.create(pk=1234, product=self.p, player=player, status='sell', quantity=1000, price=1.00)
        self.pl = player

    def _click(self, value):
        page = self.app.get(reverse('market', kwargs={'action': 'buy', 'item': 'potatoe'}), user=self.usr)
        page.form['quantity'] = value
        page = page.form.submit(name='1234').maybe_follow()
        return page

    def test_csrf(self):
        self.client.force_login(User.objects.get_or_create(username='testuser')[0])
        response = self.client.get(reverse('market', kwargs={'action': 'buy', 'item': 'potatoe'}))
        self.assertContains(response, 'csrfmiddlewaretoken')

    def test_form_success(self):
        page = self._click(500)
        self.assertIsNotNone(page.html.find("div", {"class": "alert"}))
        self.assertIsNotNone(page.html.find("div", {"class": "alert-success"}))
        self.assertIn("You bought the item with success.", page.html.find("div", {"class": "alert-success"}).get_text())

    def test_error_not_enough_money(self):
        Player.objects.select_for_update().filter(user=self.usr).update(money=400)
        page = self._click(500)
        self.assertIsNotNone(page.html.find("div", {"class": "alert"}))
        self.assertIsNotNone(page.html.find("div", {"class": "alert-warning"}))
        self.assertIn("You don't have enough money.", page.html.find("div", {"class": "alert-warning"}).get_text())
        page = self._click(350)
        self.assertIsNotNone(page.html.find("div", {"class": "alert"}))
        self.assertIsNotNone(page.html.find("div", {"class": "alert-success"}))
        self.assertIn("You bought the item with success.", page.html.find("div", {"class": "alert-success"}).get_text())
        # Must finish with 50 money and 350 products
        player = Player.objects.get(user=self.usr)
        self.assertEqual(player.money, 50)  # 400-350
        self.assertEqual(Ownership.objects.get(player=player, product=self.p).quantity, 350)

    def test_dont_exceed_offer_quantity(self):
        page = self._click(2000)  # The offer is 1000
        self.assertEqual(Ownership.objects.get(player=self.player_, product=self.p).quantity, 1000)
        with self.assertRaises(ObjectDoesNotExist):
            Offer.objects.get(pk=1234)

    def test_cant_buy_to_yourself(self):
        Offer.objects.select_for_update().filter(pk=1234).update(player=self.player_)
        page = self._click(1000)
        self.assertIsNotNone(page.html.find("div", {"class": "alert"}))
        self.assertIsNotNone(page.html.find("div", {"class": "alert-warning"}))
        self.assertIn("You can't buy to yourself.", page.html.find("div", {"class": "alert-warning"}).get_text())
        self.assertEqual(Offer.objects.get(pk=1234).quantity, 1000)


class SellItemOffersViewTests(WebTest):
    def setUp(self):
        self.usr = User.objects.create_user(username='alpha', email='john@doe.com', password='top_secret')
        self.player_ = Player.objects.get(user=self.usr)
        user = User.objects.create_user(username='john', email='john@doe.com', password='top_secret')
        player = Player.objects.get(user=user)
        self.p = Product.objects.create(name='potatoe', price=1.00, category='agri')
        Offer.objects.create(pk=1234, product=self.p, player=player, status='buy', quantity=1000, price=1.00)
        Ownership.objects.create(pk=987, player=self.player_, product=self.p, quantity=10**6)

    def _click(self, value):
        page = self.app.get(reverse('market', kwargs={'action': 'sell', 'item': 'potatoe'}), user=self.usr)
        page.form['quantity'] = value
        page = page.form.submit(name='1234').maybe_follow()
        return page

    def test_csrf(self):
        self.client.force_login(User.objects.get_or_create(username='testuser')[0])
        response = self.client.get(reverse('market', kwargs={'action': 'sell', 'item': 'potatoe'}))
        self.assertContains(response, 'csrfmiddlewaretoken')

    def test_form_success(self):
        page = self._click(500)
        self.assertIsNotNone(page.html.find("div", {"class": "alert"}))
        self.assertIsNotNone(page.html.find("div", {"class": "alert-success"}))
        self.assertIn("You sold the item with success.", page.html.find("div", {"class": "alert-success"}).get_text())

    def test_form_error_nothing_in_stock(self):
        Ownership.objects.get(pk=987).delete()
        page = self._click(500)
        self.assertIsNotNone(page.html.find("div", {"class": "alert"}))
        self.assertIsNotNone(page.html.find("div", {"class": "alert-warning"}))
        self.assertIn("You don't have this ressource.", page.html.find("div", {"class": "alert-warning"}).get_text())

    def test_form_error_not_enough_quantity(self):
        Ownership.objects.get(pk=987).delete()
        Ownership.objects.create(player=self.player_, product=self.p, quantity=200)
        page = self._click(500)
        self.assertIsNotNone(page.html.find("div", {"class": "alert"}))
        self.assertIsNotNone(page.html.find("div", {"class": "alert-warning"}))
        self.assertIn("You don't have enough ressources.", page.html.find("div", {"class": "alert-warning"}).get_text())

    def test_form_error_buyer_no_money(self):
        Player.objects.select_for_update().filter(user__username='john').update(money=400)
        page = self._click(500)
        self.assertIsNotNone(page.html.find("div", {"class": "alert"}))
        self.assertIsNotNone(page.html.find("div", {"class": "alert-warning"}))
        self.assertIn("The buyer doesn't have enough money.", page.html.find("div", {"class": "alert-warning"}).get_text())
        page = self._click(350)
        self.assertIsNotNone(page.html.find("div", {"class": "alert"}))
        self.assertIsNotNone(page.html.find("div", {"class": "alert-success"}))
        self.assertIn("You sold the item with success.", page.html.find("div", {"class": "alert-success"}).get_text())

    def test_form_error_cant_sell_to_yourself(self):
        p = Player.objects.get(user__username='john')
        Offer.objects.select_for_update().filter(pk=1234).update(player=self.player_)
        page = self._click(500)
        self.assertIsNotNone(page.html.find("div", {"class": "alert"}))
        self.assertIsNotNone(page.html.find("div", {"class": "alert-warning"}))
        self.assertIn("You can't sell to yourself.", page.html.find("div", {"class": "alert-warning"}).get_text())

    def test_form_exceed_offer_quantity(self):
        start = Ownership.objects.get(player__user=self.usr, product__name='potatoe').quantity
        page = self._click(2000)  # The offer is 1000
        end = Ownership.objects.get(player=self.player_, product=self.p).quantity
        self.assertEqual(start - end, 1000)

    def test_money_transaction(self):
        page = self._click(500)
        self.assertEqual(Player.objects.get(user=self.usr).money, 10000500)
        self.assertEqual(Player.objects.get(user__username='john').money, 9999500)

    def test_product_transaction(self):
        page = self._click(500)
        self.assertEqual(Ownership.objects.get(pk=987).quantity, 999500)
        self.assertEqual(Ownership.objects.get(player__user__username='john', product=self.p).quantity, 500)

    def test_offer_removed_when_empty(self):
        page = self._click(1000)  # The offer is 1000
        with self.assertRaises(ObjectDoesNotExist):
            Offer.objects.get(pk=1234)

    def test_ownership_removed_when_empty(self):
        Ownership.objects.select_for_update().filter(pk=987).update(quantity=500)
        page = self._click(500)
        with self.assertRaises(ObjectDoesNotExist):
            Ownership.objects.get(pk=987)
