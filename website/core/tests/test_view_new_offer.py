from django.conf import settings
from django_webtest import WebTest
from django.urls import resolve, reverse
from django.contrib.auth.models import User
from django.urls import reverse
from django.test import TestCase, RequestFactory

from ..forms import NewOfferForm
from ..views import NewOfferView
from ..models import Player, Product, Ownership


class NewOfferTests(TestCase):
    def setUp(self):
        url = reverse('new_offer')
        self.client.force_login(User.objects.get_or_create(username='testuser')[0])
        self.response = self.client.get(url)

    def test_working_app(self):
        self.assertIn('core', settings.INSTALLED_APPS)

    def test_new_offer_status_code(self):
        self.assertEqual(self.response.status_code, 200)

    def test_new_offer_url_resolves_newoffer_view(self):
        view = resolve('/new_offer/')
        self.assertEqual(view.view_name, 'new_offer')
        self.assertEqual(view.func.view_class, NewOfferView)

    def test_csrf(self):
        self.assertContains(self.response, 'csrfmiddlewaretoken')

    def test_contains_form(self):
        form = self.response.context.get('form')
        self.assertIsInstance(form, NewOfferForm)

    def test_form_inputs(self):
        self.assertContains(self.response, '<input', 3)
        self.assertContains(self.response, 'type="number"', 2)


class NewOfferViewTests(WebTest):
    def setUp(self):
        self.usr = User.objects.create_user(username='john', email='john@doe.com', password='top_secret')
        player = Player.objects.get(user=self.usr)
        self.product = Product.objects.create(name='potatoe', price=1.00, category='agri')
        Ownership.objects.create(pk=123, player=player, product=self.product, quantity=1000)
        self.offers_max = settings.OFFERS_NB_MAX

    def _click(self, action='buy', quantity=5000, price=1.00):
        page = self.app.get(reverse('new_offer'), user=self.usr)
        page.form['action'] = action
        page.form['product'].select(text=self.product.name)
        page.form['quantity'] = quantity
        page.form['price'] = price
        page = page.form.submit().maybe_follow()
        return page

    def test_form_success(self):
        page = self._click()
        self.assertIsNotNone(page.html.find("div", {"class": "alert"}))
        self.assertIsNotNone(page.html.find("div", {"class": "alert-success"}))
        self.assertIn("The offer has been posted.", page.html.find("div", {"class": "alert-success"}).get_text())

    def test_no_product_in_stock(self):
        Ownership.objects.get(pk=123).delete()
        page = self._click(action='sell')
        self.assertIsNotNone(page.html.find("div", {"class": "alert"}))
        self.assertIsNotNone(page.html.find("div", {"class": "alert-warning"}))
        self.assertIn("You don't have this product in your stock.", page.html.find("div", {"class": "alert-warning"}).get_text())

    def test_not_enough_to_sell(self):
        page = self._click(action='sell', quantity=2000)  # Own 1000
        self.assertIsNotNone(page.html.find("div", {"class": "alert"}))
        self.assertIsNotNone(page.html.find("div", {"class": "alert-warning"}))
        self.assertIn("You don't have enough ressources.", page.html.find("div", {"class": "alert-warning"}).get_text())

    def test_sell_offer_transaction(self):
        page = self._click(action='sell', quantity=500)  # Own 1000
        self.assertEqual(Ownership.objects.get(pk=123).quantity, 500)

    def test_maximum_five_buy_offers_per_player(self):
        for _ in range(self.offers_max - 1):
            self._click()
        page = self._click()
        self.assertIsNotNone(page.html.find("div", {"class": "alert-success"}))
        page = self._click()
        self.assertIsNotNone(page.html.find("div", {"class": "alert"}))
        self.assertIsNotNone(page.html.find("div", {"class": "alert-warning"}))
        self.assertIn(f"You can't post more than {self.offers_max} offers.", page.html.find("div", {"class": "alert-warning"}).get_text())

    def test_maximum_five_sell_offers_per_player(self):
        Ownership.objects.select_for_update().filter(pk=123).update(quantity=10**6)  # Don't run out of ressources
        for _ in range(self.offers_max - 1):
            self._click(action='sell')
        page = self._click(action='sell')
        self.assertIsNotNone(page.html.find("div", {"class": "alert-success"}))
        page = self._click(action='sell')
        self.assertIsNotNone(page.html.find("div", {"class": "alert"}))
        self.assertIsNotNone(page.html.find("div", {"class": "alert-warning"}))
        self.assertIn(f"You can't post more than {self.offers_max} offers.", page.html.find("div", {"class": "alert-warning"}).get_text())

    def test_maximum_offers_number_mixed(self):
        Ownership.objects.select_for_update().filter(pk=123).update(quantity=10**6)  # Don't run out of ressources
        for _ in range(self.offers_max):
            self._click(action='sell')
        for _ in range(self.offers_max):
            page = self._click()
            self.assertIsNotNone(page.html.find("div", {"class": "alert-success"}))
