from decimal import Decimal

from django_webtest import WebTest
from django.urls import resolve, reverse
from django.contrib.auth.models import User

from ..views import BuyFieldView
from ..forms import BuyFieldForm
from ..models import Player, Product, Production


class BuyFieldTests(WebTest):
    def setUp(self):
        self.response = self.app.get(reverse('buy_field', kwargs={'activity': 'farm'}), user='user')

    def test_buy_field_url_resolves_buyfield_view(self):
        activities = ('farm', 'factory', 'exploit')
        for activity in activities:
            view = resolve(f'/buy_field/{activity}/')
            self.assertEqual(view.view_name, 'buy_field')
            self.assertEqual(view.func.view_class, BuyFieldView)

    def test_csrf(self):
        self.assertContains(self.response, 'csrfmiddlewaretoken')

    def test_contains_form(self):
        form = self.response.context.get('form')
        self.assertIsInstance(form, BuyFieldForm)


class BuyFieldViewTests(WebTest):
    def setUp(self):
        self.usr = User.objects.create_user(username='john', email='john@doe.com', password='top_secret')
        self.player = Player.objects.get(user=self.usr)
        self.product = Product.objects.create(name='potatoe', price=1.00, category='agri')

    def _click(self, activity='farm', product='potatoe'):
        page = self.app.get(reverse('buy_field', kwargs={'activity': activity}), user=self.usr)
        page.form['product'].select(text=product)
        page = page.form.submit().maybe_follow()
        return page

    def test_form_success(self):
        page = self._click()
        self.assertIsNotNone(page.html.find("div", {"class": "alert"}))
        self.assertIsNotNone(page.html.find("div", {"class": "alert-success"}))
        self.assertIn("You bought a field.", page.html.find("div", {"class": "alert-success"}).get_text())
        self.assertEqual(Production.objects.filter(player__user=self.usr).count(), 1)

    def test_not_enough_money(self):
        Player.objects.select_for_update().filter(user=self.usr).update(money=1)
        page = self._click()
        self.assertIsNotNone(page.html.find("div", {"class": "alert"}))
        self.assertIsNone(page.html.find("div", {"class": "alert-success"}))
        self.assertIsNotNone(page.html.find("div", {"class": "alert-warning"}))
        self.assertIn("You don't have enough money.", page.html.find("div", {"class": "alert-warning"}).get_text())
        self.assertEqual(Production.objects.filter(player__user=self.usr).count(), 0)

    def test_buy_fields(self):
        self.assertEqual(Production.objects.filter(player=self.player).count(), 0)
        self.assertEqual(Player.objects.get(user=self.usr).fields, 0)
        self._click()
        self.assertEqual(Production.objects.filter(player=self.player).count(), 1)
        self.assertEqual(Player.objects.get(user=self.usr).fields, 1)
        # Add and buy another product
        Product.objects.create(name='milk', price=0.3, category='agri')
        self._click(product='milk')
        self.assertEqual(Production.objects.filter(player=self.player).count(), 2)
        self.assertEqual(Player.objects.get(user=self.usr).fields, 2)
        # Add industry product then buy it
        Product.objects.create(name='cheese', price=2.5, category='agro')
        Product.objects.create(name='ham', price=8, category='agro')
        self._click(activity='factory', product='cheese')
        self.assertEqual(Production.objects.filter(player=self.player).count(), 3)
        self.assertEqual(Player.objects.get(user=self.usr).fields, 3)
        # Add industry and exploitation products then buy
        Product.objects.create(name='computer', price=500, category='industrie')
        Product.objects.create(name='oil', price=75, category='exploitation')
        self._click(activity='factory', product='computer')
        self._click(activity='exploit', product='oil')
        self.assertEqual(Production.objects.filter(player=self.player).count(), 5)
        self.assertEqual(Player.objects.get(user=self.usr).fields, 5)

    def test_field_price(self):
        self._click()
        self.assertEqual(Player.objects.get(user=self.usr).money, 9000000)
        self._click()
        self.assertEqual(Player.objects.get(user=self.usr).money, 7992500)
        self._click()
        self.assertEqual(Player.objects.get(user=self.usr).money, 6977443.75)
        # Add 10 millions in player account then buy a new field
        player = Player.objects.get(user=self.usr)
        player.money += 10000000
        player.save()
        self._click()
        self.assertAlmostEqual(Player.objects.get(user=self.usr).money, Decimal(15954774.58))
