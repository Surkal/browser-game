from django_webtest import WebTest

from ..models import Transaction
from .test_form_payment import BuyItemOffersViewTests


class TransactionTests(BuyItemOffersViewTests):
    def setUp(self):
        super().setUp()

    def test_transaction_success(self):
        page = self._click(500)
        self.assertTrue(Transaction.objects.filter(buyer=self.player_, seller=self.pl).exists())
        self.assertEqual(Transaction.objects.all().count(), 1)
