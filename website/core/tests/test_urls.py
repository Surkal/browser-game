from django.http import Http404
from django.urls import reverse
from django_webtest import WebTest

from ..models import Product


class RoutingTests(WebTest):
        def test_wrong_urls(self):
            page = self.app.get(reverse('market', kwargs={'action': 'buy', 'item': 'potatoe'}), user='user', status=404)
            self.assertEqual(page.status_code, 404)
            page = self.app.get(reverse('market', kwargs={'action': 'sell', 'item': 'potatoe'}), user='user', status=404)
            self.assertEqual(page.status_code, 404)
            page = self.app.get(reverse('market', kwargs={'action': 'test', 'item': 'potatoe'}), user='user', status=404)
            self.assertEqual(page.status_code, 404)
            page = self.app.get(reverse('offers', kwargs={'action': 'test'}), user='user', status=404)
            self.assertEqual(page.status_code, 404)
            page = self.app.get(reverse('production', kwargs={'activity': 'wrong'}), user='user', status=404)
            self.assertEqual(page.status_code, 404)
            page = self.app.get(reverse('buy_field', kwargs={'activity': 'test'}), user='user', status=404)
            self.assertEqual(page.status_code, 404)

        def test_correct_urls(self):
            Product.objects.create(name='milk', price=1.00, category='agri')
            page = self.app.get(reverse('market', kwargs={'action': 'buy', 'item': 'milk'}), user='user')
            self.assertEqual(page.status_code, 200)
            page = self.app.get(reverse('market', kwargs={'action': 'sell', 'item': 'milk'}), user='user')
            self.assertEqual(page.status_code, 200)
            page = self.app.get(reverse('transaction_log'), user='user')
            self.assertEqual(page.status_code, 200)
            page = self.app.get(reverse('production', kwargs={'activity': 'farm'}), user='user')
            self.assertEqual(page.status_code, 200)
            page = self.app.get(reverse('production', kwargs={'activity': 'factory'}), user='user')
            self.assertEqual(page.status_code, 200)
            page = self.app.get(reverse('production', kwargs={'activity': 'exploit'}), user='user')
            self.assertEqual(page.status_code, 200)
            page = self.app.get(reverse('buy_field', kwargs={'activity': 'farm'}), user='user')
            self.assertEqual(page.status_code, 200)
            page = self.app.get(reverse('buy_field', kwargs={'activity': 'factory'}), user='user')
            self.assertEqual(page.status_code, 200)
            page = self.app.get(reverse('buy_field', kwargs={'activity': 'exploit'}), user='user')
            self.assertEqual(page.status_code, 200)

        def test_correct_urls_unlogged(self):
            Product.objects.create(name='milk', price=1.00, category='agri')
            page = self.client.get(reverse('market', kwargs={'action': 'buy', 'item': 'milk'}))
            self.assertEqual(page.status_code, 302)
            page = self.client.get(reverse('transaction_log'))
            self.assertEqual(page.status_code, 302)
            page = self.app.get(reverse('production', kwargs={'activity': 'farm'}))
            self.assertEqual(page.status_code, 302)
            page = self.app.get(reverse('production', kwargs={'activity': 'factory'}))
            self.assertEqual(page.status_code, 302)
            page = self.app.get(reverse('production', kwargs={'activity': 'exploit'}))
            self.assertEqual(page.status_code, 302)
            page = self.app.get(reverse('buy_field', kwargs={'activity': 'farm'}))
            self.assertEqual(page.status_code, 302)
            page = self.app.get(reverse('buy_field', kwargs={'activity': 'factory'}))
            self.assertEqual(page.status_code, 302)
            page = self.app.get(reverse('buy_field', kwargs={'activity': 'exploit'}))
            self.assertEqual(page.status_code, 302)
