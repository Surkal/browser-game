from django.urls import reverse
from django_webtest import WebTest
from django.test import TestCase, RequestFactory
from django.utils.numberformat import format as nformat
from django.contrib.auth.models import AnonymousUser, User

from ..views import home
from ..models import Player


class UserTests(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='john', email='john@doe.com', password='abcdef123')

    def test_logged_in_account_money(self):
        request = self.client.get('/')
        request.user = self.user
        response = home(request)
        self.assertEqual(response.status_code, 200)
        self.assertIn('10 000 000,00 €', response.content.decode('utf-8'))  # assertContains doesn't work, maybe because of the 'floatnumber' filter

    def test_logged_out_account_money(self):
        request = self.client.get('/')
        request.user = AnonymousUser()
        response = home(request)
        self.assertEqual(response.status_code, 200)
        self.assertNotIn('10 000 000,00 €', response.content.decode('utf-8'))


class UrlsTests(WebTest):
    def setUp(self):
        self.usr = User.objects.create_user(username='test')
        Player.objects.get(user=self.usr)

    def logged_navbar(self, page):
        """
        Every pages we see on navbar as logged user
        """
        self.assertEqual(page.click(href=reverse('offers', kwargs={'action': 'buy'})).status_code, 200)
        self.assertEqual(page.click(href=reverse('offers', kwargs={'action': 'sell'})).status_code, 200)
        self.assertEqual(page.click(href=reverse('new_offer')).status_code, 200)
        self.assertEqual(page.click(href=reverse('transaction_log')).status_code, 200)
        self.assertEqual(page.click(href=reverse('production', kwargs={'activity': 'farm'})).status_code, 200)
        self.assertEqual(page.click(href=reverse('production', kwargs={'activity': 'factory'})).status_code, 200)
        self.assertEqual(page.click(href=reverse('production', kwargs={'activity': 'exploit'})).status_code, 200)
        self.assertEqual(page.click(href=reverse('logout')).status_code, 302)
        self.assertEqual(page.click(href=reverse('stock')).status_code, 302)
        self.assertEqual(page.click(href=reverse('password_change')).status_code, 302)

    def test_navbar_urls(self):
        """
        Check if the navbar is working on every pages we see it.
        """
        self.logged_navbar(self.app.get(reverse('home'), user='user'))
        self.logged_navbar(self.app.get(reverse('stock'), user=self.usr))
        self.logged_navbar(self.app.get(reverse('offers', kwargs={'action': 'buy'}), user='user'))
        self.logged_navbar(self.app.get(reverse('offers', kwargs={'action': 'sell'}), user='user'))
        self.logged_navbar(self.app.get(reverse('production', kwargs={'activity': 'farm'}), user='user'))
        self.logged_navbar(self.app.get(reverse('production', kwargs={'activity': 'factory'}), user='user'))
        self.logged_navbar(self.app.get(reverse('production', kwargs={'activity': 'exploit'}), user='user'))
        self.logged_navbar(self.app.get(reverse('transaction_log'), user='user'))
        self.logged_navbar(self.app.get(reverse('new_offer'), user='user'))
