from faker import Factory
from django.urls import reverse
from django_webtest import WebTest
from django.contrib.auth.models import User

from ..models import Transaction, Product, Player


class TransactionLogViewTests(WebTest):
    def setUp(self):
        self.fake = Factory.create()
        self.usr = User.objects.create_user(username=self.fake.profile()['username'])
        u = User.objects.create_user(username=self.fake.profile()['username'])
        u_ = User.objects.create_user(username=self.fake.profile()['username'])
        p1 = Player.objects.get(user=self.usr)
        self.p2 = Player.objects.get(user=u)
        self.p3 = Player.objects.get(user=u_)
        self.p = Product.objects.create(name='potatoe', price=1.00, category='agri')
        Transaction.objects.create(seller=p1, buyer=self.p2, product=self.p, quantity=1000, price=1.00)
        Transaction.objects.create(seller=self.p2, buyer=p1, product=self.p, quantity=1000, price=1.00)


    def test_view_success(self):
        page = self.app.get(reverse('transaction_log'), user=self.usr)
        self.assertNotEqual(page.html.tbody.get_text()[:-1], '')
        self.assertEqual(len(page.html.find('tbody').find_all('tr')), 2)
        page = self.app.get(reverse('transaction_log'), user='test')
        self.assertEqual(page.html.tbody.get_text()[:-1], '')

    def test_filter_among_all_transactions(self):
        Transaction.objects.create(seller=self.p2, buyer=self.p3, product=self.p, quantity=1000, price=1.00)
        page = self.app.get(reverse('transaction_log'), user=self.usr)
        self.assertNotEqual(page.html.tbody.get_text()[:-1], '')
        self.assertEqual(len(page.html.find('tbody').find_all('tr')), 2)
        page = self.app.get(reverse('transaction_log'), user='test')
        self.assertEqual(page.html.tbody.get_text()[:-1], '')
