from django.test import TestCase

from ..forms import NewOfferForm
from ..views import Product


class NewOfferFormTests(TestCase):
    def setUp(self):
        Product.objects.create(id=123, name='patate', price=1, category='Agriculture')

    def test_form_has_fields(self):
        form = NewOfferForm()
        expected = ['action', 'product', 'quantity', 'price']
        actual = list(form.fields)
        self.assertSequenceEqual(expected, actual)

    def test_valid_form(self):
        data_form = {'action': 'sell', 'product': '123', 'quantity': '500', 'price': '2.00'}
        form = NewOfferForm(data_form)
        self.assertTrue(form.is_valid())
        data_form['action'] = 'buy'
        self.assertTrue(NewOfferForm(data_form).is_valid())

    def test_invalid_form(self):
        # float quantity
        data_form = {'action': 'sell', 'product': '123', 'quantity': '500.1', 'price': '2.00'}
        self.assertFalse(NewOfferForm(data_form).is_valid())
        # unknown product
        data_form = {'action': 'sell', 'product': '1234', 'quantity': '500', 'price': '2.00'}
        self.assertFalse(NewOfferForm(data_form).is_valid())
        # unknown action
        data_form = {'action': 'action', 'product': '123', 'quantity': '500', 'price': '2.00'}
        self.assertFalse(NewOfferForm(data_form).is_valid())
