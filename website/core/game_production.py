import os
import sys

import django
#from django.db.models import F
from django.db.utils import DatabaseError
from django.db import transaction, IntegrityError
from django.core.exceptions import ObjectDoesNotExist

sys.path.append('/src/website')
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'website.settings')
django.setup()

from core.models import Production, Product, Ownership, ProductComponent


def main():
    for x in Production.objects.all():
        # TODO: too long for a 'try'
        try:
            with transaction.atomic():
                o, _ = Ownership.objects.get_or_create(player=x.player, product=x.product)
                o.quantity += x.product.productivity
                o.save()
                ingredients = x.product.get_ingredients()
                if not ingredients:
                    continue
                for p in ingredients:
                    i = ProductComponent.objects.get(product=x.product, ingredient=p)
                    o_ = Ownership.objects.get(player=x.player, product=p)
                    o_.quantity -= i.quantity * i.product.productivity
                    o_.save()
        except IntegrityError:
            pass


if __name__ == '__main__':
    main()
