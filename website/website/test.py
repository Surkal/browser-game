from django.conf import settings
from django.test import TestCase

class SimpleTest(TestCase):

    def test_working_apps(self):
        self.assertIn('accounts', settings.INSTALLED_APPS)
        self.assertIn('core', settings.INSTALLED_APPS)
